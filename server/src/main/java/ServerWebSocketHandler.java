import org.eclipse.jetty.websocket.api.*;
import org.eclipse.jetty.websocket.api.annotations.*;
import java.io.*;

@WebSocket
public class ServerWebSocketHandler {

    @OnWebSocketConnect
    public void onConnect(Session session) {
        String username = "User" + Server.nextUserNumber ++;
        Server.users.put(session, username);
        Server.broadcastMessage(null, username + " joined the chat");
    }

    @OnWebSocketClose
    public void onClose(Session session, int statusCode, String reason) {
        String username = Server.users.get(session);
        Server.users.remove(session);
        Server.broadcastMessage(null, username + " left the chat");
    }

    @OnWebSocketMessage
    public void onMessage(Session session, String message) throws IOException {
        Server.broadcastMessage(session, message);
    }

}