import org.eclipse.jetty.websocket.api.Session;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;


public class Server {

    static Map<Session, String> users = new HashMap<>();
    static int nextUserNumber = 1;

    public static void main(String[] args) {

        try {
            port(Integer.parseInt(System.getenv("PORT")));
            webSocket("/chat", ServerWebSocketHandler.class);
            get("/port", ((request, response) -> System.getenv("PORT")));
        } catch (Exception e) {
            port(80); //localhost
            webSocket("/chat", ServerWebSocketHandler.class);
            init();
        }

    }

    public static void broadcastMessage(Session sender, String message) {

        String user = (sender == null) ? "Server" : users.get(sender);
        for(Session s : users.keySet()) {
            try {
                if(s != sender)
                    s.getRemote().sendString(user + ": " + message + "\nYour message: ");
                else
                    s.getRemote().sendString("Your message: ");
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println(user + ": " + message);
    }
}