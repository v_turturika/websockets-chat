import java.net.URI;

import java.util.Scanner;

import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;


public class Client {

    public static void main(String[] args) {

        if(args.length < 1) {
            System.out.println("Usage: client host port");
            return;
        }

        String destUri = "ws://" + args[0]  + "/chat/";

        WebSocketClient client = new WebSocketClient();
        ClientWebSocketHandler socket = new ClientWebSocketHandler();
        try {

            client.start();
            URI echoUri = new URI(destUri);
            ClientUpgradeRequest request = new ClientUpgradeRequest();
            client.connect(socket, echoUri, request);

            Scanner input = new Scanner(System.in);
            String msg = "";

            while (!msg.equals("Bye")) {
                msg = input.nextLine();
                socket.sendMessage(msg);
            }

        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            try {
                client.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}