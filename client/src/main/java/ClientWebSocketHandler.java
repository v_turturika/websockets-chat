import java.io.IOException;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;

@WebSocket(maxTextMessageSize = 64 * 1024)
public class ClientWebSocketHandler {

    private Session session;

    public void sendMessage(String msg) {

        if ( session != null ) {
            try {
                session.getRemote().sendString(msg);
            }
            catch (IOException e) {
                e.getMessage();
            }
        }
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.println("Connection closed");
        session = null;
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        System.out.println("Connection start");
        this.session = session;
    }

    @OnWebSocketMessage
    public void onMessage(String msg) {
        System.out.print("\r" + msg);
    }
}