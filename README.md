# WebSockets Console Chat #

Console chat via websockets. Used [spark server](http://sparkjava.com/), [jetty client websockets api](http://www.eclipse.org/jetty/documentation/current/jetty-websocket-client-api.html). Written in Java 8. Posted on [Heroku](https://www.heroku.com).

## How to use? ##

### By local computer ###

* Start server
```
#!java
java -jar server.jar
```

* Start client (one or more)
```
#!java
java -jar client.jar localhost
```

### By internet ###

Start client (one or more)
```
#!java
java -jar client.jar turturika-websocket-chat.herokuapp.com
```

For stop conversation type ```Bye```